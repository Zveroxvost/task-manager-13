package ru.t1.zvero.tm.api.service;

import ru.t1.zvero.tm.enumerated.Status;
import ru.t1.zvero.tm.model.Project;
import ru.t1.zvero.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task create(String name, String description);

    Task create(String name);

    Task add(Task task);

    boolean existsById(String id);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    void clear();

    void remove(Task task);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}