package ru.t1.zvero.tm.api.repository;

import ru.t1.zvero.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}