package ru.t1.zvero.tm.api.repository;

import ru.t1.zvero.tm.model.Project;
import ru.t1.zvero.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    boolean existsById(String id);

    List<Project> findAll();

    List<Project> findAllByProjectId(String projectId);

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

}