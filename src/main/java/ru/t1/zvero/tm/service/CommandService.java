package ru.t1.zvero.tm.service;

import ru.t1.zvero.tm.api.repository.ICommandRepository;
import ru.t1.zvero.tm.api.service.ICommandService;
import ru.t1.zvero.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}