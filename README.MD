# TASK MANAGER

## DEVELOPER INFO

**NAME**: Elena Volnenko

**EMAIL**: elena@volnenko.ru

**EMAIL**: nujkova@yandex.ru

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: MACOS MONTEREY 12.1

## HARDWARE

**CPU**: i7

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM

```bash
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
